1) создаем rpm: python setup.py bdist_rpm 
2) в /dist : myhw-0.1-1.noarch.rpm
3) устанавливаем: rpm -i myhw-0.1-1.noarch.rpm
4) копируем в systemd: hw.service in /etc/systemd/system/ (systemctl daemon-reload systemctl start hw.service systemctl enable hw.service)

5) открываем и смотрим http://localhost:8080/
6) смотрим логи journalctl -u hw.service

Все тестировалось в CentOS Linux release 7.4.1708 (Core) + python 2.7
