from setuptools import setup

setup(
      name='myhw',
      version='0.1' ,
      description='my test work',
      author='Anton Verlan',
      author_email='averlan@yandex.ru',
      python_requires=['Python>=2.0'],
      packages=['myhw']
)
